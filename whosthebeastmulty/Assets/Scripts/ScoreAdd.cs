﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreAdd : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        var joueur = other.gameObject;
        var Scored = joueur.GetComponent<Score>();
        Scored.AddScore(Scored.rmChampy() * 300);
    }
}
