﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public int ScorePhase = 0;
    public static int ScoreTotal = 0;
    public static int nbChampignon = 0;
    public Text ScoreText;
    string SceneActuelle;
    

    public void changeScene()
    {
        if (SceneActuelle == "Mini-Jeu")
        {
            NetworkManager.singleton.ServerChangeScene("Mini-Jeu2");
        }
        else if (SceneActuelle == "Mini-Jeu2")
        {
            NetworkManager.singleton.ServerChangeScene("PhaseFinale");
        }
    }

    public void AddScore(int amount)
    {
        ScoreTotal += amount;
        ScorePhase += amount;
        if (ScorePhase>=5000)
        {
            ScorePhase = 0;
            changeScene();
        }

        string changeText = "Score Total : " + ScoreTotal + "\n" + "Score Phase : " + ScorePhase;
        ScoreText.text = changeText;
    }

    public void AddChampy()
    {
        nbChampignon++;
        string changeText = "Score Total : " + ScoreTotal + "\n" + "Score Phase : " + ScorePhase ;
        changeText += "\n" + "Champignons : " + nbChampignon;
        ScoreText.text = changeText;
        
    }

    public int rmChampy()
    {
        int nb = nbChampignon;
        nbChampignon = 0;
        return nb;
    }
    // Start is called before the first frame update
    void Start()
    {
        SceneActuelle = SceneManager.GetActiveScene().name;
    }
    
}
