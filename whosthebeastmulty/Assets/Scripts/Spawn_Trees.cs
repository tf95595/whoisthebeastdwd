﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Trees : MonoBehaviour
{
    public GameObject Tree;
    public int xPos;
    public int zPos;
    
    void Start()
    {
        StartCoroutine(TreeDrop());
    }

    IEnumerator TreeDrop()
    {
        zPos = 0;
        for (int i = 0; i < 50; i+= 4)
        {
            xPos = i;
            Instantiate(Tree, new Vector3(xPos, 0, zPos), Quaternion.identity);
            yield return new WaitForSeconds(0.05f);
        }
        
        xPos = 0;
        for (int i = 0; i < 50; i+= 4)
        {
            zPos = i;
            Instantiate(Tree, new Vector3(xPos, 0, zPos), Quaternion.identity);
            yield return new WaitForSeconds(0.05f);
        }
        
        zPos = 50;
        for (int i = 0; i < 50; i+= 4)
        {
            xPos = i;
            Instantiate(Tree, new Vector3(xPos, 0, zPos), Quaternion.identity);
            yield return new WaitForSeconds(0.05f);
        }
        xPos = 50;
        for (int i = 0; i < 50; i+=4)
        {
            zPos = i;
            Instantiate(Tree, new Vector3(xPos, 0, zPos), Quaternion.identity);
            yield return new WaitForSeconds(0.05f);
        }
        
        
    }
}
