﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBallon : MonoBehaviour
{
    
    public void OnCollisionEnter(Collision other)
    {
        var hit = other.gameObject;
        var Scored = hit.GetComponent<Score>();
        Scored.AddChampy();
        Destroy(this.gameObject);
    }
    
}
