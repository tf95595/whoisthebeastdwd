﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Health : NetworkBehaviour
{
   public const int MaxHealth = 100;
   [SyncVar(hook = "OnChangeHealth")]
   public  int currentHealth = MaxHealth;
   
   public RectTransform healthBar;
   

   public void TakeDamage(int amount)
   {
      currentHealth -= amount;
      if (currentHealth<0)
      {
         currentHealth = 0;
         Debug.Log("Dead!");
      }
   }

   void OnChangeHealth(int health)
   {
      healthBar.sizeDelta = new Vector2(health,healthBar.sizeDelta.y);
   }
   
}
