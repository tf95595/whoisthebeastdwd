﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnBalls : MonoBehaviour
{
    public GameObject balle;
    public GameObject herbe;
    public int xPos;
    public int zPos;
    public int x1Pos;
    public int z1Pos;
    public int ballCount;
    void Start()
    {
        StartCoroutine(BallDrop());
    }

    // Update is called once per frame

    IEnumerator BallDrop()
    {
        while (ballCount < 17)
        {
            xPos = Random.Range(10, 40);
            zPos = Random.Range(10, 40);
            x1Pos = Random.Range(10, 40);
            z1Pos = Random.Range(10, 40);
            Instantiate(balle, new Vector3(xPos, 0.9f, zPos), Quaternion.identity);
            Instantiate(herbe, new Vector3(x1Pos, 0.3f, z1Pos), Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
            ballCount++;
        } 
    }
}
