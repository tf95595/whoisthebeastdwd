﻿
using System;
using UnityEngine;
using UnityEngine.Networking;


public class playercontroller : NetworkBehaviour
{
    public int speed ;
    public int turnspeed ;
    public int stamina;
    public int MaxStamina;
    public Vector3 jumpspeed;
    public int staminaReg;
    private CapsuleCollider playerCollider;
    bool IsGrounded()
    {
        return Physics.CheckCapsule(playerCollider.bounds.center,
            new Vector3(playerCollider.bounds.center.x, playerCollider.bounds.min.y - 0.1f,
                playerCollider.bounds.center.z), 0.7f);
    }

    private void Start()
    {
        playerCollider = gameObject.GetComponent<CapsuleCollider>();
        if (!isLocalPlayer)
        {
            transform.Find("Camera").gameObject.SetActive(false);
            transform.Find("Audio Listener").gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        if (stamina >0 && Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKey("z") )
            {
                transform.Translate(0,0,2 * speed/3 * Time.deltaTime);
                stamina -= staminaReg * 5;
            }
            if (Input.GetKey("s") )
            {
                transform.Translate(0,0,-speed/3 * Time.deltaTime);
                stamina -= staminaReg * 5;
            }
        }
        if (!Input.GetKey(KeyCode.LeftShift) || stamina <=0)
        {
            if (Input.GetKey("z"))
            {
                transform.Translate(0,0,speed * Time.deltaTime);
            }
            if (Input.GetKey("s"))
            {
                transform.Translate(0,0,-speed/2 * Time.deltaTime);
            }
        }
        
        stamina = stamina + staminaReg;
        stamina = Mathf.Clamp(stamina, 0, MaxStamina);
        
        
        if (Input.GetKey("d"))
        {
            transform.Rotate(0,turnspeed,0);
        }
        if (Input.GetKey("q"))
        {
            transform.Rotate(0,-turnspeed,0);
        }

        

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            Vector3 v = gameObject.GetComponent<Rigidbody>().velocity;
            v.y = jumpspeed.y;
            gameObject.GetComponent<Rigidbody>().velocity = jumpspeed;
        }
        
        
        /*var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        
        transform.Rotate(0,x,0);
        transform.Translate(0,0, z);*/
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }
}
